#!/usr/bin/python3
# -- coding: utf-8 --

import calcoohija
import csv

if __name__ == "__main__":

    with open('fichero.csv') as csvfile:

        fichero = csv.reader(csvfile)

        for linea in fichero:

            print(linea[0])
            calculo = linea[0]
            result = linea[1]

            for operandos in linea[2:]:
                objeto = calcoohija.Calculadorahija(result, calculo, operandos)

                result = objeto.operacion()

            print(result)
