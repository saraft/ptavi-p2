#!/usr/bin/python3
# -- coding: utf-8 --


import sys


class Calculadora():
    def __init__(self, valor1, op, valor2):
        self.valor1 = valor1
        self.op = op
        self.valor2 = valor2

    def suma(self):
        return int(self.valor1) + int(self.valor2)

    def resta(self):
        return int(self.valor1) - int(self.valor2)


class Calculadorahija(Calculadora):

    def division(self):
        if int(self.valor2) == 0:
            return ("Division by zero is not allowed")
        else:
            return int(self.valor1) / int(self.valor2)

    def multiplicacion(self):
        return int(self.valor1) * int(self.valor2)

    def operacion(self):
        if self.op == 'division':
            return int(self.division())
        elif self.op == 'multiplicacion':
            return self.multiplicacion()
        elif self.op == 'suma':
            return self.suma()
        elif self.op == 'resta':
            return self.resta()
        else:
            return ('no valeee esta operacion')

if __name__ == "__main__":

    try:
        valor1 = sys.argv[1]
        valor2 = sys.argv[3]
        op = sys.argv[2]

    except ValueError:
        sys.exit("error")

    objeto = Calculadorahija(int(valor1), op, int(valor2))
    solucion = objeto.operacion()
    print(solucion)
