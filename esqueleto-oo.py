#!/usr/bin/python3
# -*- coding: utf-8 -*-

class Clase(ClaseMadre):
  "Esto es un ejemplo de clase que hereda de ClaseMadre" #si no pone nada no hereda de nadie y está vacio

  def __init__(self, valor): #valor variable local
    "Esto es el método iniciliazador"
    self.atributo = valor #variables self atributos global dento de la clase
#aqui acaba lo de la clase

if __name__ == "__main__":
  objeto = Clase("pepe")     # Creo un objeto de la clase Clase
                             # y le paso el valor pepe para su
                             # atributo en la inicialización

objeto2 = clase("maria")