#!/usr/bin/python3
# -- coding: utf-8 --


import calcoohija

if __name__ == "__main__":

    fichero = open('fichero.csv')
    lineas = fichero.readlines()

    for linea in lineas:
        datos = linea.split(",")
        print(datos[0])
        calculo = datos[0]
        result = datos[1]

        for operandos in datos[2:]:
            objeto = calcoohija.Calculadorahija(result, calculo, operandos)

            result = objeto.operacion()

        print(result)

    fichero.close()
